import SimpleSchema from 'simpl-schema';


SimpleSchema.login = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: 'Email.',
        optional: false
    },
    password: {
        type: String,
        autoform: {
            type: 'password'
        },
        label: 'Mot de passe.',
        optional: false
    },
});


SimpleSchema.register = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: 'Email.',
        optional: false
    },
    password: {
        type: String,
        label: 'Mot de passe.',
        optional: false,
        autoform: {
            type: 'password'
        },
    },
    passwordVerif: {
        type: String,
        label: 'Mot de passe vérification.',
        optional: false,
        autoform: {
            type: 'password'
        },
        custom: function () {
            if (this.value !== this.field('password').value) {
                return "password Missmatch";
            }
        }
    },
});