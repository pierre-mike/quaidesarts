import {FilesCollection} from 'meteor/ostrio:files';
import SimpleSchema from 'simpl-schema';


SimpleSchema.dimension = new SimpleSchema({
    hauteur: {
        label: 'Hauteur en cm',
        type: Number,
        optional: false,
    },
    largeur: {
        type: Number,
        label: 'Largeur en cm',
        optional: false,

    },
    profondeur: {
        type: Number,
        label: 'Profondeur en cm',
        optional: true,

    },
});


SimpleSchema.meta = new SimpleSchema({
    nom: {
        type: String,
        optional: false,
        max: 28
    },
    description: {
        type: String,
        optional: false,
        autoform: {
            rows: 5
        }
    },
    dimension: {
        type: SimpleSchema.dimension,
        optional: false,
    }

});

SimpleSchema.Image = {
    _id: {
        type: String
    },
    size: {
        type: Number,
        optional: true,
    },
    name: {
        type: String,
        optional: true,
    },
    type: {
        type: String
    },
    path: {
        type: String
    },
    isVideo: {
        type: Boolean
    },
    isAudio: {
        type: Boolean
    },
    isImage: {
        type: Boolean
    },
    isText: {
        type: Boolean
    },
    isJSON: {
        type: Boolean
    },
    isPDF: {
        type: Boolean
    },
    extension: {
        type: String,
        optional: true
    },
    ext: {
        type: String,
        optional: true
    },
    extensionWithDot: {
        type: String,
        optional: true
    },
    mime: {
        type: String,
    },
    'mime-type': {
        type: String,
    },
    _storagePath: {
        type: String
    },
    _downloadRoute: {
        type: String
    },
    _collectionName: {
        type: String
    },
    public: {
        type: Boolean,
        optional: true
    },
    meta: {
        type: SimpleSchema.meta,
        optional: false
    },
    userId: {
        type: String,
        optional: true
    },
    updatedAt: {
        type: Date,
        optional: true
    },
    versions: {
        type: Object,
        blackbox: true
    }
};


Images = new FilesCollection({
    collectionName: 'Images',
    storagePath: Meteor.settings.public.storagePath,
    permissions: 0o774,
    parentDirPermissions: 0o774,
    allowClientCode: true, // Required to let you remove uploaded file
    onBeforeUpload(file) {
        // test si la personne est connecté
        if (!Meteor.userId()) {
            return 'vous este pas connecté';
        }
        if (Images.find({userId: Meteor.userId()}).count() >= 5) {
            return 'vous avez uploader le nombre maximum d\'images (5)';
        }
        // Allow upload files under 10MB, and only in png/jpg/jpeg formats
        if (file.size <= 10485760 && /png|jpg|jpeg/i.test(file.ext)) {
            return true;
        } else {
            return 'Please upload image, with size equal or less than 10MB';
        }
    }
});


Images.collection.attachSchema(SimpleSchema.Image);

