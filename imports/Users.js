import SimpleSchema from 'simpl-schema';

SimpleSchema.extendOptions(['autoform']);

SimpleSchema.Pos = new SimpleSchema({
    lat: {
        label: 'latitude',
        type: Number,
        optional: true,
        autoform: {
            readonly: true
        }
    },
    lng: {
        label: 'longitude',
        type: Number,
        optional: true,
        autoform: {
            readonly: true
        }
    }
});


SimpleSchema.Address = new SimpleSchema({
    adresse: {
        label: 'adresse',
        type: String,
        max: 100,
        autoform: {
            class: 'addressAtelier'
        }
    },
    precision: {
        label: 'precision',
        type: String,
        max: 100,
        optional: true
    }
});

SimpleSchema.Perso = new SimpleSchema({
    nom: {
        type: String,
        optional: false,
        min: 3,
        max: 30,
        label: 'Nom',
    },
    prenom: {
        type: String,
        optional: false,
        min: 3,
        max: 30,
        label: 'Prenom'
    },
    adresse: {
        label: 'Adresse domicile',
        type: String,
        max: 100,
        autoform: {
            class: 'adressePerso'
        }
    },
    tel: {
        type: String,
        label: 'Tétéphone',
        optional: false,
        regEx: /^[0-9]{10}$/,
    },
    description: {
        type: String,
        optional: false,
        label: 'Description du travail et du parcours artistique',
        autoform: {
            rows: 5
        }
    },
    artisteNom: {
        type: String,
        optional: false,
        label: 'Nom d\'artiste'
    },
    discipline: {
        type: String,
        optional: false,
        allowedValues: ['huile', 'numerique', 'acrylique', 'pastel', 'sculpture', 'aquarelle', 'autre'],
        autoform: {
            options: [
                {label: "Huile", value: "huile"},
                {label: "Art Numérique", value: "numerique"},
                {label: "Acrylique", value: "acrylique"},
                {label: "Autres techniques", value: "autre"},
                {label: "Pastel", value: "pastel"},
                {label: "Sculpture", value: "sculpture"},
                {label: "Aquarelle", value: "aquarelle"},
            ]
        }
    },
    site: {
        type: String,
        optional: true,
        regEx: SimpleSchema.RegEx.Url,
        label: "Site web",
    },
    quai: {
        label: "J'expose sur les Quais",
        type: Boolean,
        optional: false,
    },
    atelier: {
        label: "J'ouvre mon atelier",
        type: Boolean,
        optional: false,
    },
    horaires: {
        label: "Horaires d'ouvertures",
        type: String,
        optional: true,
        autoform: {
            rows: 5
        }
    }
});

SimpleSchema.Profile = new SimpleSchema({
    perso: {
        type: SimpleSchema.Perso,
        optional: false,
        label: 'Profile',
    },
    address: {
        type: SimpleSchema.Address,
        label: 'Adresse de mon atelier',
    },
    zone: {
        type: String,
        label: 'Secteur souhaité',
        allowedValues: ['rose', 'vert', 'bleu', 'noir'],
        optional: false,
        autoform: {
            options: [
                {label: "Rose, Pop art, Street art", value: "rose"},
                {label: "Vert, Sculture", value: "vert"},
                {label: "Bleu, Peinture", value: "bleu"},
                {label: "Noir, Zone Barnum", value: "noir"},
            ]
        }
    },
    posAtelier: {
        label: 'position',
        type: SimpleSchema.Pos,
        optional: true,
    },
    posQuai: {
        label: 'position',
        type: SimpleSchema.Pos,
        optional: true,
    },
    validate: {
        label: 'valider',
        type: Boolean,
        optional: true,
    },
    validatePos: {
        label: 'valider',
        type: Boolean,
        optional: true,
    },
    token: {
        type: String,
        optional: true
    },
    etape: {
        type: Number,
        optional: true,
    }
});

SimpleSchema.User = new SimpleSchema({
    _id: {
        type: String
    },
    createdAt: {
        type: Date
    },
    emails: {
        type: Array,
        optional: false
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: 'Votre email.',
    },
    "emails.$.verified": {
        type: Boolean,
        optional: true
    },
    profile: {
        type: SimpleSchema.Profile,
        optional: true,
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    roles: {
        type: Array,
        optional: true
    },
    "roles.$": {
        type: String
    }

});


Meteor.users.attachSchema(SimpleSchema.User);


