FlowRouter.route('/', {
    action: function () {
        BlazeLayout.render('appLayout', {main: 'description', header: 'header'});
    }
});

FlowRouter.route('/connection', {
    action: function () {
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'connection', header: 'header'});
    }
});

FlowRouter.route('/profile', {
    action: function () {
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'profile', header: 'header'});
    }
});

FlowRouter.route('/quai', {
    action: function () {
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'mapQuai', header: 'header'});
    }
});



FlowRouter.route('/ateliers', {
    action: function () {
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'ateliers', header: 'header'});
    }
});

FlowRouter.route('/photos', {
    action: function () {
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'photos', header: 'header'});
    }
});


FlowRouter.route('/artistes', {
    action: function () {
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'artistes', header: 'header'});
    }
});

FlowRouter.route('/artiste/:id', {
    action: function (params) {
        Session.set('artisteSelected', params.id);
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'artiste', header: 'header'});
    }
});


FlowRouter.route('/artistesAdmin', {
    action: function () {
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'artistesAdmin', header: 'header'});
    }
});


FlowRouter.route('/artistesPosition', {
    action: function () {
        // SI connecté -> profile
        BlazeLayout.render('appLayout', {main: 'artistesPosition', header: 'header'});
    }
});



Accounts.onLogin(function () {
    if (Roles.userIsInRole(Meteor.user(), 'admin')) {
        FlowRouter.go('/artistesAdmin');
    } else if (Roles.userIsInRole(Meteor.user(), 'artiste')) {
        FlowRouter.go('/profile');
    }
    // Seems a bit too simple? more on this later!
})