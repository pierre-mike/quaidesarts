Meteor.methods({
    register(email, password) {
        var id = Accounts.createUser({
            email: email,
            password: password,
        });
        Roles.addUsersToRoles(id, 'artiste');

    },
    updateProfile(profile) {
        profile.modifier.$set['profile.etape'] = 1;
        console.log(profile.modifier);
        Meteor.users.update(Meteor.userId(), profile.modifier);
    },
    updateZone(profile) {
        if (profile.modifier.$set['profile.perso.atelier'] === true) {
            var newAdresse = profile.modifier.$set['profile.address.adresse'];
            if (newAdresse) {
                var result = geo.geocode(newAdresse);
                profile.modifier.$set['profile.posAtelier.lat'] = result[0].latitude;
                profile.modifier.$set['profile.posAtelier.lng'] = result[0].longitude;
            }
        }
        profile.modifier.$set['profile.etape'] = 2;
        console.log(profile.modifier);
        Meteor.users.update(Meteor.userId(), profile.modifier);
    },
    updateQuai(profile) {
        profile.modifier.$set['profile.etape'] = 3;
        console.log(profile.modifier);
        Meteor.users.update(Meteor.userId(), profile.modifier);
    },
    getUsers() {
        if (Roles.userIsInRole(Meteor.user(), 'admin')) {
            return Roles.getUsersInRole('artiste').fetch();
        } else {
            return [];
        }

    },
    validateUser(userId) {
        if (Roles.userIsInRole(Meteor.user(), 'admin')) {
            Meteor.users.update({_id: userId}, {$set: {'profile.validate': true}});
            var userEmail = Meteor.users.findOne({_id: userId}).emails[0].address;
            Meteor.call('sendEmail', userEmail, 'Avis favorable pour Quai des Arts', Meteor.settings.public.msgValidate)
        }
    },
    validateUserPos(userId) {
        if (Roles.userIsInRole(Meteor.user(), 'admin')) {
            console.log('envoie de maol validation pos')
            Meteor.users.update({_id: userId}, {$set: {'profile.validatePos': true}});
            var userEmail = Meteor.users.findOne({_id: userId}).emails[0].address;
            console.log(Meteor.settings.public.msgValidatePos);
            Meteor.call('sendEmail', userEmail, 'Avis favorable pour Quai des Arts', Meteor.settings.public.msgValidatePos)
        }
    },
    invalidateUser(userId) {
        if (Roles.userIsInRole(Meteor.user(), 'admin')) {
            Meteor.users.update({_id: userId}, {
                $unset: {
                    'profile.validate': '',
                    'profile.posQuai': '',
                    'profile.validatePos': '',
                }
            })
        }
    },
    sendEmail(to, subject, text) {
        // Make sure that all arguments are strings.
        check([to, subject, text], [String]);
        // Let other method calls from the same client start running, without
        // waiting for the email sending to complete.
        this.unblock();
        Email.send({to, 'from': 'expo-abia@abia.fr', subject, text});
    },
    insertImages(file, meta) {

    },
    etapeRetour() {
        if (Meteor.user().profile.etape > 0) {
            Meteor.users.update(Meteor.userId(), {$inc: {'profile.etape': -1}});
        }
    },
    etapeSuivant() {
        if (Meteor.user().profile.etape === 3 && Images.find({userId: Meteor.userId()}).count() === 5) {
            Meteor.users.update(Meteor.userId(), {$inc: {'profile.etape': 1}});
        } else if (Meteor.user().profile.etape === 4 && Meteor.user().profile.validate) {
            Meteor.users.update(Meteor.userId(), {$inc: {'profile.etape': 1}});
        }
    },
    etapePaiement() {
        if (Meteor.user().profile.etape === 4 && Meteor.user().profile.perso.validate) {
            Meteor.users.update(Meteor.userId(), {$inc: {'profile.etape': 1}});
        }
    },
    updateMeta(doc) {
        Images.collection.update(doc._id, doc.modifier);
    },
    paiement(token) {
        if (Roles.userIsInRole(Meteor.user(), 'artiste')) {
            stripe.charges.create({
                amount: 2000,
                currency: 'eur',
                description: 'Paiement de l\'inscription',
                source: token,
                metadata: {userId: Meteor.userId()},

            }, Meteor.bindEnvironment(function (err, charge) {
                if (err) {
                    throw err
                } else {
                    Meteor.users.update(Meteor.userId(), {
                        $set: {'profile.validate': true, 'profile.token': token},
                        $inc: {'profile.etape': 1}
                    });
                    return true
                }
            }));
        }
    },
    updatePosQuai(userId, latLng) {
        if (Roles.userIsInRole(Meteor.user(), 'admin')) {
            Meteor.users.update(userId, {
                $set: {
                    'profile.posQuai.lat': latLng.lat,
                    'profile.posQuai.lng': latLng.lng,
                }
            });
        }
    }

});