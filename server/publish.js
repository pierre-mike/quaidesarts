// Server: Publish the `Rooms` collection, minus secret info...
Meteor.publish(
    'Users', function () {
        return Meteor.users.find({});
    });


Meteor.publish(
    'ImagesArtiste', function () {
        if (Roles.userIsInRole(Meteor.user(), 'artiste')) {
            return Images.find({userId: Meteor.userId()}).cursor;
        } else {
            return Images.find().cursor;
        }
    });

