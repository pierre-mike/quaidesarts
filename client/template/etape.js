Template.etape.helpers({
    //add you helpers here
    pourcent() {
        let etape = Meteor.user().profile.etape;
        if (!etape) {
            return 1
        } else {
            return etape * 20;
        }

    },
    etape1() {
        if (Meteor.user().profile.etape === 0) {
            return Meteor.user().profile.etape
        }
        return Meteor.user().profile.etape;
    },
    etape2() {
        return Meteor.user().profile.etape === 1
    },
    etape3() {
        return Meteor.user().profile.etape === 2
    },
    etape4() {
        return Meteor.user().profile.etape === 3
    },
    etape5() {
        return Meteor.user().profile.etape === 4
    },
    etape6() {
        return Meteor.user().profile.etape === 5
    }
});

Template.etape.events({
    'click .retour': function (e, t) {
        Meteor.call('etapeRetour');
        //add your events here
    }
});

Template.etape.onCreated(function () {
    //add your statement here
});

Template.etape.onRendered(function () {
    //add your statement here
});

Template.etape.onDestroyed(function () {
    //add your statement here
});

