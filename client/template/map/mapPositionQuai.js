Template.mapPositionQuai.helpers({
    //add you helpers here

    mapPositionQuaiOptions() {
        if (GoogleMaps.loaded()) {
            // Map initialization options
            return {
                center: new google.maps.LatLng(47.22290541911459, -1.5528273582458496),
                zoom: 17,
                styles: [
                    {
                        "featureType": "poi",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    }
                ]
            };
        }
    },
});

Template.mapPositionQuai.events({
    //add your events here
});

Template.mapPositionQuai.onCreated(function () {
    GoogleMaps.ready('mapPublic', function (map) {
        // mettre a jour sa position
        Tracker.autorun(function () {

        });    //add your statement here
    });
    //add your statement here
});

Template.mapPositionQuai.onRendered(function () {
    //add your statement here
});

Template.mapPositionQuai.onDestroyed(function () {
    //add your statement here
});

