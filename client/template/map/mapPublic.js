Template.mapPublic.helpers({
    publicOptions() {
        if (GoogleMaps.loaded()) {
            // Map initialization options
            return {
                center: new google.maps.LatLng(47.2172500, -1.5533600),
                styles: [
                    {
                        "featureType": "poi",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    }
                ],
                zoom: 12
            };

        }
    },
    getAteliers() {
        return Meteor.users.find({'profile.perso.atelier': {$ne: true}})
    }


    //add you helpers here
});

Template.mapPublic.events({
    //add your events here
});

Template.mapPublic.onCreated(function () {
    Meteor.subscribe('Users');
    Meteor.subscribe('ImagesArtiste');

    GoogleMaps.ready('mapPublic', function (map) {
        var infowindow = new google.maps.InfoWindow();


        // mettre a jour sa position
        Tracker.autorun(function () {


            var users = Meteor.users.find({'profile.perso.atelier': true, 'profile.validate': true}).fetch()
            users.forEach(function (user) {


                var marker = new google.maps.Marker({
                    position: user.profile.posAtelier,
                    id: user._id,
                    horaires: user.profile.perso.horaires,
                    map: map.instance,
                    icon: "/painting-palette.png",
                });
                if (!user.profile.perso.horaires) {
                    user.profile.perso.horaires = 'Aucun horaire renseigné';
                }

                marker.addListener('click', function (e) {
                    console.log(marker.get("horaires"));
                    infowindow.setContent(user.profile.perso.horaires + " <br> <a href=\"artiste/" + user._id + "\">" + user.profile.perso.artisteNom + "</a>");
                    infowindow.open(map, marker);

                });


            });
        });    //add your statement here
    });
});
Template.mapPublic.onRendered(function () {
    //add your statement here
});

Template.mapPublic.onDestroyed(function () {
    //add your statement here
});

