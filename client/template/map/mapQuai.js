Template.mapQuai.helpers({
    mapQuai() {
        if (GoogleMaps.loaded()) {
            // Map initialization options
            return {
                center: new google.maps.LatLng(47.22290541911459, -1.5528273582458496),
                zoom: 17,
                styles: [
                    {
                        "featureType": "poi",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    }
                ]
            };
        }
    },
    //add you helpers here
});

Template.mapQuai.events({
    //add your events here
});

Template.mapQuai.onCreated(function () {

    GoogleMaps.ready('mapQuai', function (map) {
        // CREATION DES MARKER AUTOMATIQUE
        Tracker.autorun(function () {
            var markers = Meteor.users.find({'profile.posQuai': {$ne: null}}).fetch()


            markers.forEach(function (user) {
                console.log(user.profile.posQuai);
                var marker = new google.maps.Marker({
                    position: user.profile.posQuai,
                    map: map.instance,
                    label: user.profile.perso.artisteNom,
                });
            })
        }); //add your statement here
    });


    //add your statement here
});

Template.mapQuai.onRendered(function () {
    //add your statement here
});

Template.mapQuai.onDestroyed(function () {
    //add your statement here
});

