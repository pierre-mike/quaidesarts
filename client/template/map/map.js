Template.map.helpers({
    exampleMapOptions: function () {
        // Make sure the maps API has loaded
        if (GoogleMaps.loaded()) {
            // Map initialization options
            var posAtelier = Meteor.user().profile.posAtelier;
            if (posAtelier) {
                return {
                    center: new google.maps.LatLng(posAtelier.lat, posAtelier.lng),
                    zoom: 16,
                    styles: [
                        {
                            "featureType": "poi",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        }
                    ],

                };
            }
            return {
                center: new google.maps.LatLng(47.2172500, -1.5533600),
                zoom: 16,
                styles: [
                    {
                        "featureType": "poi",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    }
                ],
            };
        }
    },
    //add you helpers here
});

Template.map.events({
    //add your events here
});

Template.map.onCreated(function () {
    //add your statement here
    GoogleMaps.ready('map', function (map) {
        // mettre a jour sa position
        Tracker.autorun(function () {
            var marker = new google.maps.Marker({
                position: Meteor.user().profile.posAtelier,
                map: map.instance,
            });
            map.instance.panTo(Meteor.user().profile.posAtelier);
        });
    });
});

Template.map.onRendered(function () {

});

Template.map.onDestroyed(function () {
    //add your statement here
});

