Template.viewUser.helpers({
    getUsers() {
        return Meteor.users.find({'profile.validate': {$eq: true}})
    },
    getUsersNotValidate() {
        return Meteor.users.find({'profile.validate': {$ne: true}, _id: {$ne: Meteor.userId()}})
    },
    isValide(user) {
        return user.profile.validate === true ? 'success' : 'disabled';
    },
    getEmail(user) {
        return user.emails[0].address;
    }

    //add you helpers here
});

Template.viewUser.events({
    'click .user'(e, t) {
        console.log(e);
        Session.set('userId', e.currentTarget.id);
        $('#view').modal('toggle');
    }
    //add your events here
});

Template.viewUser.onCreated(function () {
    //add your statement here
});

Template.viewUser.onRendered(function () {
    //add your statement here
});

Template.viewUser.onDestroyed(function () {
    //add your statement here
});

