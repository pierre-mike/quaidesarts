Template.view.helpers({
    //add you helpers here
    images() {
        return Images.find({userId: Session.get('userId')}).cursor;
    },
    valide() {
        console.log('test')
        return Meteor.users.findOne({_id: Session.get('userId')}).profile.validate === true ? true : false
    },

});

Template.view.events({
    'click #valider': function (e, t) {
        Meteor.call('validateUser', Session.get('userId'));
    },
    'click #validerPos': function (e, t) {
        console.log('valider pos')
        Meteor.call('validateUserPos', Session.get('userId'));
    },
    'click #invalider': function (e, t) {
        Meteor.call('invalidateUser', Session.get('userId'));
    },

    //add your events here
});

Template.view.onCreated(function () {
    Meteor.subscribe('Users');
    Meteor.subscribe('ImagesArtiste');
    //add your statement here
});

Template.view.onRendered(function () {
    //add your statement here
});

Template.view.onDestroyed(function () {
    //add your statement here
});

