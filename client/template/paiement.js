import {Meteor} from "meteor/meteor";

Template.paiement.helpers({
    validate() {
        return Meteor.user().profile.validate
    },
    paiementEnCour() {
        return Session.get('verifToken')
    },
    //add you helpers here
});

Template.paiement.events({
    'click .suivant'(e, t) {
        console.log('test')
        Meteor.call('etapeSuivant')
    }
});

Template.paiement.onCreated(function () {
    //add your statement here
    Session.set('verifToken', false);
});

Template.paiement.onRendered(function () {
    //add your statement here


    var handler = StripeCheckout.configure({
        key: Meteor.settings.public.stripe.testKey,
        image: '/abia.jpg',
        locale: 'auto',
        name: 'abia',
        description: 'Paiement de l\'inscription',
        currency: 'eur',
        amount: 2000,
        opened: function () {
            $("#pay").toggle();
            $("#payDisabled").toggle();
        },
        closed: function () {
            if (!Session.get('verifToken')) {
                $("#pay").toggle();
                $("#payDisabled").toggle();
            }
        },
        token: function (token) {
            Session.set('verifToken', true);

            Meteor.call('paiement', token.id, function (error, result) {
                Session.set('verifToken', false);
                if (error) {
                    Bert.alert(error.toString(), 'danger', 'growl-top-right')
                    $("#pay").toggle();
                    $("#payDisabled").toggle();
                }
                if (result) {
                    Bert.alert('Paiement éffectué', 'success', 'growl-top-right')
                }
            })
        }
    });

    document.getElementById('pay').addEventListener('click', function (e) {
        handler.open();
        e.preventDefault();
    });

    /*$("#pay").click(function (e) {
        handler.open();
        e.preventDefault();
    })*/

// Close Checkout on page navigation:
    window.addEventListener('popstate', function () {
        handler.close();
    });
});

Template.paiement.onDestroyed(function () {
    //add your statement here

});

