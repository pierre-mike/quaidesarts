Template.recapitulatif.helpers({
    user() {
        return Meteor.user()
    },
    suivant() {
        return Meteor.user().profile.validate === true ? '' : 'disabled';
    }
    //add you helpers here
});

Template.recapitulatif.events({
    'click .paiement'() {
        Meteor.call('etapePaiement');
    }
    //add your events here
});

Template.recapitulatif.onCreated(function () {
    //add your statement here
});

Template.recapitulatif.onRendered(function () {
    //add your statement here
});

Template.recapitulatif.onDestroyed(function () {
    //add your statement here
});

