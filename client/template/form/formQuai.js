Template.formQuai.helpers({
    //add you helpers here
    user() {
        return Meteor.user()
    },
    choixQuai() {
        return AutoForm.getFieldValue('profile.perso.quai')
    },
    choixZoneNoir() {
        return AutoForm.getFieldValue('profile.zone') === 'noir'
    },
});

Template.formQuai.events({
    //add your events here
});

Template.formQuai.onCreated(function () {
    //add your statement here
});

Template.formQuai.onRendered(function () {
    //add your statement here
});

Template.formQuai.onDestroyed(function () {
    //add your statement here
});

