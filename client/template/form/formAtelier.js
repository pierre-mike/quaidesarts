Template.formAtelier.helpers({
    user() {
        return Meteor.user()
    },
    choixAtelier() {
        return AutoForm.getFieldValue('profile.perso.atelier')
    },
    //add you helpers here
});

Template.formAtelier.events({

    //add your events here
});

Template.formAtelier.onCreated(function () {
    //add your statement here
});

Template.formAtelier.onRendered(function () {
    //add your statement here
});

Template.formAtelier.onDestroyed(function () {
    //add your statement here
});

