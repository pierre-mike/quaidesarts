import SimpleSchema from "simpl-schema";

Template.upload.helpers({
    currentUpload() {
        return Template.instance().currentUpload.get();
    },
    nbMax() {
        return !(Images.find({userId: Meteor.userId()}).count() >= 5)
    },
    schema() {
        return SimpleSchema.meta
    }
    //add you helpers here
});

Template.upload.events({
    'submit #insertImages'(e, t) {
        e.preventDefault();

        var file = t.find('input[type="file"]').files[0];
        if (file) {
            var description = t.find('textarea[name="description"]').value;
            var nom = t.find('input[name="nom"]').value;
            var hauteur = t.find('input[name="dimension.hauteur"]').value;
            var largeur = t.find('input[name="dimension.largeur"]').value;
            var profondeur = t.find('input[name="dimension.profondeur"]').value;
            var meta = {
                'nom': nom, 'description': description, 'dimension': {
                    'largeur': largeur,
                    'hauteur': hauteur,
                    'profondeur': profondeur
                }
            };
            console.log(meta);


            const upload = Images.insert({
                file: file,
                streams: 'dynamic',
                chunkSize: 'dynamic',
                meta: meta
            }, false);
            upload.on('start', function () {
                t.currentUpload.set(this);
            });

            upload.on('end', function (error, fileObj) {
                if (error) {
                    console.log('Error during upload: ' + error);
                } else {
                    $('#uploadOeuvre').modal('toggle');
                    Bert.alert('Images envoyé', 'success', 'growl-top-right');
                }
                t.currentUpload.set(false);
            });

            upload.start();

        } else {
            Bert.alert('veuillez choisir une image', 'danger', 'growl-top-right');
        }


        // SimpleSchema.meta.validate(meta);
    },
    'click .suivant'(e, t) {
        Meteor.call('etapeSuivant')
    }
    //add your events here
});

Template.upload.onCreated(function () {
    this.currentUpload = new ReactiveVar(false);
    //add your statement here
});

Template.upload.onRendered(function () {

    //add your statement here
});

Template.upload.onDestroyed(function () {
    //add your statement here
});

