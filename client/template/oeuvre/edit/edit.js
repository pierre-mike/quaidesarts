Template.edit.helpers({
    getOeuvre() {
        return Images.collection.findOne(Session.get('oeuvreId'));
    }
});

Template.edit.events({
    //add your events here
});

Template.edit.onCreated(function () {
    //add your statement here
});

Template.edit.onRendered(function () {
    //add your statement here
});

Template.edit.onDestroyed(function () {
    //add your statement here
});

