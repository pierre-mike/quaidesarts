Template.mesoeuvres.helpers({
    oeuvres() {
        return Images.find({userId: Meteor.userId()}).cursor;
    },
    nbOeuvre() {
        return Images.find({userId: Meteor.userId()}).count();
    }
    //add you helpers here
});

Template.mesoeuvres.events({
    'click .edit-oeuvre'(e, t) {
        Session.set('oeuvreId', e.currentTarget.id);
        $('#edit-oeuvre').modal('toggle');
    },
    'click .deleteImage'(e, t) {
        var id = e.currentTarget.id;
        Images.remove(id, function (err) {
            if (err) {
                Bert.alert(err.toString(), 'danger', 'growl-top-right')
            } else {
                Bert.alert('Images supprimé', 'success', 'growl-top-right');
            }
        });
    }
});

Template.mesoeuvres.onCreated(function () {
    Meteor.subscribe('ImagesArtiste');
    //add your statement here
});

Template.mesoeuvres.onRendered(function () {
    //add your statement here
});

Template.mesoeuvres.onDestroyed(function () {
    //add your statement here
});

