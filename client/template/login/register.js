import SimpleSchema from "simpl-schema";

Template.register.helpers({
    //add you helpers here
    schema() {
        return SimpleSchema.register
    }
});

Template.register.events({
    'submit #register'(e, t) {
        e.preventDefault();
        // retrieve the input field values
        var email = t.find('input[name="email"]').value;
        var password = t.find('input[name="password"]').value;
        var passwordVerif = t.find('input[name="passwordVerif"]').value;
        if (passwordVerif != password) {
            Bert.alert('Verifier le mot de passe', 'danger', 'growl-top-right')
        } else {
            Meteor.call('register', email, password, function (err) {
                if (err) {
                    Bert.alert(err.toString(), 'danger', 'growl-top-right')
                } else {
                    Bert.alert('votre compte est créé', 'success', 'growl-top-right')
                    Meteor.loginWithPassword(email, password, function (err) {
                        if (err) {
                            Bert.alert(err.toString(), 'danger', 'growl-top-right')
                        }
                    });
                }
            });
        }


        return false;
    }
    //add your events here
});

Template.register.onCreated(function () {
    //add your statement here
});

Template.register.onRendered(function () {
    //add your statement here
});

Template.register.onDestroyed(function () {
    //add your statement here
});

