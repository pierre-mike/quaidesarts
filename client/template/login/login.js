import SimpleSchema from "simpl-schema";

Template.login.helpers({
    //add you helpers here
    schema() {

        return SimpleSchema.login

    }
});

Template.login.events({
    'submit #login': function (e, t) {
        e.preventDefault();
        // retrieve the input field values
        var email = t.find('input[name="email"]').value;
        var password = t.find('input[name="password"]').value;

        Meteor.loginWithPassword(email, password, function (err) {
            if (err) {
                Bert.alert(err.toString(), 'danger', 'growl-top-right')
            }
        });

    },
    //add your events here
});

Template.login.onCreated(function () {
    //add your statement here
});

Template.login.onRendered(function () {
    //add your statement here
});

Template.login.onDestroyed(function () {
    //add your statement here
});

