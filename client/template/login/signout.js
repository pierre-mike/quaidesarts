Template.signout.helpers({
    //add you helpers here
});

Template.signout.events({
    //add your events here
    'click #signout-button': function (e, t) {
        e.preventDefault();
        Meteor.logout();
        FlowRouter.go('/');
    },
});

Template.signout.onCreated(function () {
    //add your statement here
});

Template.signout.onRendered(function () {
    //add your statement here
});

Template.signout.onDestroyed(function () {
    //add your statement here
});

