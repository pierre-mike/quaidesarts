import '../imports/routes'
import '../imports/Users'
import '../imports/Images'
import '../imports/client/Scheama'

Meteor.startup(function () {
    Meteor.subscribe('Users');



    AutoForm.addHooks(null, {
        onSuccess: function (formType, result) {
            this.event.preventDefault();

            Bert.alert('succès', 'success', 'growl-top-right')
        },
        // Called when any submit operation fails
        onError: function (formType, error) {
            console.log(error);
            Bert.alert(error.message, 'danger', 'growl-top-right')
        },
        beginSubmit: function () {

        },
        endSubmit: function () {
        }
    });

    GoogleMaps.load({v: '3', key: Meteor.settings.public.keyMap, libraries: 'geometry,places'});


});