Template.artistesPosition.helpers({
        mapPositionOption() {
            if (GoogleMaps.loaded()) {
                // Map initialization options
                return {
                    center: new google.maps.LatLng(47.22290541911459, -1.5528273582458496),
                    zoom: 17,
                    styles: [
                        {
                            "featureType": "poi",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        }
                    ],
                };
            }
        },
        getUsersNotPlaced() {
            return Meteor.users.find({
                'profile.validate': {$eq: true},
                'profile.validatePos': {$eq: null}
            }, {sort: {'profile.zone': 1}})
        },
        getUsersPlaced() {
            return Meteor.users.find({
                'profile.validate': {$eq: true},
                'profile.validatePos': {$ne: null}
            }, {sort: {'profile.zone': 1}})
        },
        getColor(user) {
            return user.profile.zone;
        },
        getPlaced(user) {
            return user.profile.posQuai != null ? 'glyphicon-check' : 'glyphicon-map-marker';
        },

    }
);

Template.artistesPosition.events({
    //add your events here
    'click .user'(e, t) {
        Session.set('posUserId', e.currentTarget.id);
        Bert.alert('Selected', 'success', 'growl-top-right')


        allMarkers.forEach(function (marker) {
                if (marker.id === e.currentTarget.id) {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                } else {
                    marker.setAnimation(null);
                }
            }
        )
    }
});

Template.artistesPosition.onCreated(function () {
    //add your statement here
    Meteor.subscribe('Users');
    allMarkers = [];

    GoogleMaps.ready('mapPosition', function (map) {
        // CREATION DES MARKER AUTOMATIQUE
        Tracker.autorun(function () {
            var markers = Meteor.users.find({'profile.posQuai': {$ne: null}});



            markers.forEach(function (user) {
                var marker = new google.maps.Marker({
                    position: user.profile.posQuai,
                    map: map.instance,
                    label: user.profile.perso.artisteNom,
                });

                marker.addListener('click', function (e) {
                    Session.set('userId', marker.id);
                    $('#view').modal('toggle');
                });
                marker.set("id", user._id);
                allMarkers.push(marker)
            })


        });

        // CLIQUE DROIT CREATE
        google.maps.event.addListener(map.instance, 'rightclick', function (e) {
            if (Session.get('posUserId')) {
                Meteor.call('updatePosQuai', Session.get('posUserId'), e.latLng.toJSON())
                allMarkers.forEach(function (marker) {

                    if (marker.id === Session.get('posUserId')) {
                        marker.setMap(null);
                        var i = allMarkers.indexOf(marker);
                        if (i != -1) {
                            allMarkers.splice(i, 1);
                        }
                    }
                })
            } else {
                Bert.alert('Aucun artiste selectionné', 'warning', 'growl-top-right')
            }

        });

    });
});

Template.artistesPosition.onRendered(function () {
    //add your statement here

});

Template.artistesPosition.onDestroyed(function () {
    //add your statement here
});

