Template.profile.helpers({
    userMail() {
        return Meteor.user().emails[0].address
    },
    user() {
        return Meteor.user()
    },

    schema() {
        return SimpleSchema.User
    },
    etape1() {
        if (Meteor.user().profile.etape === 0) {
            return Meteor.user().profile.etape
        }
        return Meteor.user().profile.etape;
    },
    etape2() {
        return Meteor.user().profile.etape === 1
    },
    etape3() {
        return Meteor.user().profile.etape === 2
    },
    etape4() {
        return Meteor.user().profile.etape === 3
    },
    etape5() {
        return Meteor.user().profile.etape === 4
    },
    etape6() {
        return Meteor.user().profile.etape === 5
    }
});

Template.profile.events({
    'focus .addressAtelier'(e) {
        if (GoogleMaps.loaded()) {
            var input = document.getElementsByName('profile.address.adresse')[0];
            var options = {
                language: 'fr',
                componentRestrictions: {country: 'fr'}
            };
            autocomplete = new google.maps.places.Autocomplete(input, options);

        }
    },
    'focus .adressePerso'(e) {
        if (GoogleMaps.loaded()) {
            var input = document.getElementsByClassName('adressePerso')[0];
            var options = {
                language: 'fr',
                componentRestrictions: {country: 'fr'}
            };
            autocomplete = new google.maps.places.Autocomplete(input, options);

        }
    },

    //add your events here
});

Template.profile.onCreated(function () {
    //add your statement here
});

Template.profile.onRendered(function () {
    //add your statement here
});

Template.profile.onDestroyed(function () {
    //add your statement here
});

